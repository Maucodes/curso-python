#coding: utf-8
#author: mau


class Entidad():

    def __init__(self,nombre,apallido_paterno,apellido_materno,edad,rut):
        self.__control_errores = {"nombre": False,
                                "papellido": False,
                                "mapellido": False,
                                "edad": False,
                                "rut": False}

        self.set_nombre(nombre)
        self.set_apellido_paterno(apallido_paterno)
        self.set_apellido_materno(apellido_materno)
        self.set_edad(edad)
        self.set_rut(rut)



    def __mis_errores(self):
        return "El error es no haberlo intentado"

    def estado_clase(self):
        if True in self.__control_errores.values():
            return "hay un error en la clase"
        else:
            return "La clase se encuentra excelente"


    def set_nombre(self,nombre):
        if len(nombre) >= 3:
            self.__nombre = nombre
        else:
            self.__nombre = ""
            self.__control_errores["nombre"] = True

    def set_apellido_paterno(self,apaterno):
        if len(apaterno) >= 3:
            self.__apellido_paterno = apaterno
        else:
            self.__apellido_paterno = ""
            self.__control_errores["papellido"] = True

    def set_apellido_materno(self,amaterno):
        if len(amaterno) >= 3:
            self.__apellido_materno = amaterno
        else:
            self.__apellido_materno = ""
            self.__control_errores["mapellido"] = True

    def set_edad(self,edad):
        if edad >= 18 and edad <= 122:
            self.__edad=edad
        else:
            self.__edad = 0
            self.__control_errores["edad"] = True

    def set_rut(self,rut):
        base = "00.000.000-0"
        if (len(rut) >= len(base)):
            self.__rut = ""
        else:
            self.__rut = rut
            self.__control_errores["rut"] = True

    def info(self):
        return """
        Rut: {0}
        Nombre: {1}
        Apellidos: {2} {3}
        Edad: {4}
        Estado: {5}
        Error: {6}
        """.format(self.__rut, self.__nombre, self.__apellido_paterno, self.__apellido_materno, self.__edad, self.estado_clase(),
                   self.__mis_errores())


