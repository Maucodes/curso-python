#coding: utf-8
#author: mau
#hija

from Entidad import Entidad

class Persona(Entidad):

    def __init__(self,rut,nombre,direccion,celular,apellido_paterno,apellido_materno,telefono=""):
        Entidad.__init__(self,rut,nombre,direccion,celular,telefono)
        self.apellido_paterno = apellido_paterno
        self.apellido_materno = apellido_materno

    def info(self):
        return super().info() + " Apellidos: {0} {1}".format(self.apellido_paterno,self.apellido_materno)
    @property
    def apellido_materno(self):
        return self.__apellido_materno

    @apellido_materno.setter
    def apellido_materno(self, value):
        self.__apellido_materno = value

    @property
    def apellido_paterno(self):
        return self.__apellido_paterno

    @apellido_paterno.setter
    def apellido_paterno(self, value):
        self.__apellido_paterno = value


obj = Persona("12312-32","3123123","323232","23123123","sdasdas","asdasd")
print(obj.info())