#coding: utf-8
#author: mau

#padre

class Entidad():
    def __init__(self,rut,nombre,direccion,celular,telefono=""):
        self.rut= rut
        self.nombre = nombre
        self.direccion = direccion
        self.celular = celular
        self.telefono = telefono

    def info(self):
        return "Rut: {0}, Nombre: {1}".format(self.rut,self.nombre)

    @property
    def telefono(self):
        return self.__telefono
    @telefono.setter
    def telefono(self,telefono):
        self.__telefono = telefono

    @property
    def celular(self):
        return self.__celular
    @celular.setter
    def celular(self,celular):
        self.__celular = celular

    @property
    def direccion(self):
        return self.__direccion
    @direccion.setter
    def direccion(self,direccion):
        self.__direccion = direccion

    @property
    def nombre(self):
        return self.__nombre
    @nombre.setter
    def nombre(self,nombre):
        self.__nombre = nombre
        
    @property
    def rut(self):
        return self.__rut

    @rut.setter
    def rut(self,rut):
        self.__rut = rut

