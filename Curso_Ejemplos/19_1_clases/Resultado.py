#coding: utf-8
#author: mau

# nombre tiene que ser mayor a 3 letras
# la edad tiene que se rmayor a 18 y menor a 122
# el apellido tiene que estar separados por atriburos para los 2 apellidos
# completar la informacion visual de como se represente

class Entidad():

    def __init__(self,nombre,apallido_paterno,apellido_materno,edad,rut):
        self.control_errores = {"nombre": False,
                                "papellido": False,
                                "mapellido": False,
                                "edad": False,
                                "rut": False}

        self.set_nombre(nombre)
        self.set_apellido_paterno(apallido_paterno)
        self.set_apellido_materno(apellido_materno)
        self.set_edad(edad)
        self.set_rut(rut)



    def estado_clase(self):
        if True in self.control_errores.values():
            return "hay un error en la clase"
        else:
            return "La clase se encuentra excelente"


    def set_nombre(self,nombre):
        if len(nombre) >= 3:
            self.nombre = nombre
        else:
            self.nombre = ""
            self.control_errores["nombre"] = True

    def set_apellido_paterno(self,apaterno):
        if len(apaterno) >= 3:
            self.apellido_paterno = apaterno
        else:
            self.apellido_paterno = ""
            self.control_errores["papellido"] = True

    def set_apellido_materno(self,amaterno):
        if len(amaterno) >= 3:
            self.apellido_materno = amaterno
        else:
            self.apellido_materno = ""
            self.control_errores["mapellido"] = True

    def set_edad(self,edad):
        if edad >= 18 and edad <= 122:
            self.edad=edad
        else:
            self.edad = 0
            self.control_errores["edad"] = True

    def set_rut(self,rut):
        base = "00.000.000-0"
        if (len(rut) >= len(base)):
            self.rut = ""
        else:
            self.rut = rut
            self.control_errores["rut"] = True

    def info(self):
        return """
        Rut: {0}
        Nombre: {1}
        Apellidos: {2} {3}
        Edad: {4}
        Estado: {5}
        """.format(self.rut,self.nombre,self.apellido_paterno,self.apellido_materno,self.edad,self.estado_clase())





if __name__ == "__main__":
    usuario_1 = Entidad("Carlos","Dex","l",144,"00.000.000.000-0")
    print(usuario_1)








