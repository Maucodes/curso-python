#coding: utf-8
#author: mau

# nombre tiene que ser mayor a 3 letras
# la edad tiene que se rmayor a 18 y menor a 122
# el apellido tiene que estar separados por atriburos para los 2 apellidos
# completar la informacion visual de como se represente

# try except_ sentencias if, encapsulamiento,
class Entidad():
    def __init__(self,nombre,apellido,xfcu1):
        self.nombre = nombre
        self.apellido = apellido
        self.set_edad(xfcu1)



    def informacion(self):
        print("""
        Nombre: {0}
        Apellido: {1}
        Edad: {2}
        """.format(self.nombre,self.apellido,self.edad))

    def get_nombre(self):
        return self.nombre

    def set_nombre(self,nombre):
        self.nombre = nombre

    def set_edad(self,edad):
        if edad >= 18:
            self.edad = edad
        else:
            self.edad = 0
            print("No se aceptan menores de edad")


if __name__ == "__main__":
    usuario = Entidad("Antonio","Perez",18)
    usuario2 = Entidad("Marco", "Juanito", 8)

    usuario.informacion()
    usuario2.informacion()