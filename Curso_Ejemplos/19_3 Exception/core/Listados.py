#coding: utf-8
#author: mau
from core.Errores import Errores
class Listados():

    def __init__(self):
        self.__lista_entidades = []

    def agregar_usuario(self,usuario):
        if not(usuario.estado()):
            self.__lista_entidades.append(usuario)
        else:
            raise Errores("Entidades")


    def elimianr_usuario(self,usuario):
        self.__lista_entidades.remove(usuario)

    def busca_indice(self,indice):
        return self.__lista_entidades[indice]

    def buscar_nombre(self,nombre):
        for elemento in self.__lista_entidades:
            if elemento.get_nombre() == nombre:
                return elemento.info()
        return None

    def info(self):
        info = ""
        for elemento in self.__lista_entidades:
            info += elemento.info()

        return info
