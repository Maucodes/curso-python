#coding: utf-8
#author: mau


class Entidad():

    def __init__(self,nombre,apallido_paterno,apellido_materno,edad,rut):
        self.__control_errores = {"nombre": False,
                                "papellido": False,
                                "mapellido": False,
                                "edad": False,
                                "rut": False}

        self.set_nombre(nombre)
        self.set_apellido_paterno(apallido_paterno)
        self.set_apellido_materno(apellido_materno)
        self.set_edad(edad)
        self.set_rut(rut)

    def get_nombre(self):
        return self.__nombre

    def get_control(self):
        return self.__control_errores

    def estado(self):
        return True in self.__control_errores.values()

    def __mis_errores(self):
        return "El error es no haberlo intentado"

    def estado_clase(self):
        if True in self.__control_errores.values():
            return "hay un error en la clase"
        else:
            return "La clase se encuentra excelente"


    def set_nombre(self,nombre):
        self.__nombre = ""
        try:
            if len(nombre) >= 3:
                self.__nombre = nombre
            else:
                self.__control_errores["nombre"] = True
        except TypeError as error:
            print("Error en nombre: {0}".format(type(error)))
            self.__control_errores["nombre"] = True
        else:
            print("Se genero nombre correctamente")
        finally:
            print("Me ejecuta siempre no importa la condicion")

    def set_apellido_paterno(self,apaterno):
        self.__apellido_paterno = ""
        try:

            if len(apaterno) >= 3:
                self.__apellido_paterno = apaterno
            else:
                self.__control_errores["papellido"] = True
        except TypeError as error:
            print("Error de apellido paterno")
            self.__control_errores["papellido"] = True
        else:
            print("Se genero appelido paterno correctamente")

    def set_apellido_materno(self,amaterno):
        self.__apellido_materno = ""
        try:
            if len(amaterno) >= 3:
                self.__apellido_materno = amaterno
            else:
                self.__control_errores["mapellido"] = True
        except TypeError as error:
            self.__control_errores["mapellido"] = True
            print("Error apellido materno: ",type(error))
        else:
            print("Se genero apellido materno correctamente")

    def set_edad(self,edad):
        self.__edad = 0
        try:
            if edad >= 18 and edad <= 122:
                self.__edad=edad
            else:
                self.__control_errores["edad"] = True
        except TypeError as error:
            self.__control_errores["edad"] = True
            print("Error Edad: ", type(error))
        else:
            print("Se genero edad correctamente")

    def set_rut(self,rut):
        self.__rut = ""
        try:
            base = "00.000.000-0"
            if (len(rut) >= len(base)):
                self.__rut = rut
            else:
                self.__control_errores["rut"] = True
        except TypeError as error:
            self.__control_errores["rut"] = True
            print("Error rut: ", type(error))
        else:
            print("Se genero rut correctamente")

    def info(self):
        if not(self.estado()):
            return """
            Rut: {0}
            Nombre: {1}
            Apellidos: {2} {3}
            Edad: {4}
            Estado: {5}
            Error: {6}
            """.format(self.__rut, self.__nombre, self.__apellido_paterno, self.__apellido_materno, self.__edad, self.estado_clase(),
                       self.__mis_errores())
        else:
            return "clase no completa"


