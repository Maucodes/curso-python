#coding: utf-8
#author: mau

#nombre
#descr
#cantidad
#fecha_ ingreso
#reglas de neogicios
#todos los string tienen que ser mayor a 3
#la descripcion puede estar basia.

class Objetos():
    def __init__(self,nombre, cantidad,fecha_ingreso,descr = ""):
        self.nombre = nombre
        self.cantidad = cantidad
        self.fecha_ingreso = fecha_ingreso
        self.descr = descr

    def __str__(self):
        return """
        Info:
        Nombre: {0}
        Descr: {1}
        cantidad: {2}
        Fecha_ingres: {3}""".format(self.nombre,self.descr,self.cantidad,self.fecha_ingreso)


    @property
    def descr(self):
        return self.__descr

    @descr.setter
    def descr(self,descr):
        try:
            if len(descr)>= 3:
                self.__descr = descr
            else:
                raise TypeError
        except TypeError as error:
            print("Valor Incorrecto: {0} Tipo de Error:{1}".format(descr, type(error)))
            self.__descr = ""

    def fecha_ingreso(self):
        return self.__fecha_ingreso
    fecha_ingreso = property(fecha_ingreso)

    @fecha_ingreso.setter
    def fecha_ingreso(self,fecha):
        try:
            if len(fecha) >= 3:
                self.__fecha_ingreso = fecha
            else:
                raise TypeError
        except TypeError as error:
            print("Valor Incorrecto: {0} Tipo de Error:{1}".format(fecha, type(error)))
            self.__feca_ingreso = ""

    @property
    def cantidad(self):
        return self.__cantidad

    @cantidad.setter
    def cantidad(self,valor):
        try:
            if valor >= 1:
                self.__cantidad = valor
            else:
                raise TypeError
        except TypeError as error:
            print("Valor Incorrecto: {0} Tipo de Error:{1}".format(valor,type(error)))
            self.__cantidad = 0

    @property
    def nombre(self):
        return self.__nombre

    @nombre.setter
    def nombre(self,valor):
        try:
            if len(valor) >= 3:
                self.__nombre = valor
            else:
                raise TypeError
        except TypeError as error:
            print("Valor Incorrecto: {0} Tipo de Error:{1}".format(valor, type(error)))
            self.__nombre = ""